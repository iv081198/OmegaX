"use strict";

import MaterialInput from "./material-input";

let materialInputName = new MaterialInput(
    document.getElementById("material-input-name"),
    {
        activeClass: "material-input_active"
    }
);

let materialInputPhone = new MaterialInput(
    document.getElementById("material-input-phone"),
    {
        activeClass: "material-input_active",
        phone: true
    }
);

let materialTextArea = new MaterialInput(
    document.getElementById("material-textarea"),
    {
        activeClass: "material-textarea_active",
        textarea: true
    }
);

ymaps.ready(init);
function init(){
    // Создание карты.
    let myMap = new ymaps.Map("ymap", {
        // Координаты центра карты.
        // Порядок по умолчанию: «широта, долгота».
        // Чтобы не определять координаты центра карты вручную,
        // воспользуйтесь инструментом Определение координат.

        center: [64.547284, 40.574424],
        // Уровень масштабирования. Допустимые значения:
        // от 0 (весь мир) до 19.
        zoom: 17
    });

    myMap.behaviors.disable('scrollZoom');

    myMap.geoObjects.add(new ymaps.Placemark([64.547284, 40.574424], {
        //balloonContent: 'цвет <strong>носика Гены</strong>',
        iconCaption: 'Прокат авто "Омега Х"'
    }, {
        preset: 'islands#redAutoIcon'
    }))
}