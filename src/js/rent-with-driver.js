"use strict";

import Swiper from "swiper";

let swipers =document.querySelectorAll(
    ".driver-catalog-item__swiper"
);

swipers.forEach(container => {
    let arrowLeft = container.querySelector("#arrow-left");
    let arrowRight = container.querySelector("#arrow-right");
    let swiper = new Swiper(container , {
        wrapperClass: "driver-catalog-item__swiper-wrapper",
        slideClass: "driver-catalog-item__swiper-image",
        loop: true,
        navigation: {
            nextEl: arrowRight,
            prevEl: arrowLeft,
        }
    });
});
