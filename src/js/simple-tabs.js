"use strict";

class SimpleTabs {
    constructor(tabButtonsSelector, tabsWrapperSelector, options) {
        this.buttons = document.querySelectorAll(tabButtonsSelector);
        this.wrapper = document.querySelector(tabsWrapperSelector);
        this.options = options;

        this.activeTabNumber = options.defaultTab ? options.defaultTab : 0;
        if(this.buttons.length && this.wrapper.children.length) {
            this.activeButton = this.buttons[this.activeTabNumber];
            this.activeContent = this.wrapper.children[this.activeTabNumber];
            this.buttonActiveClass = this.buttons[0].classList[0] + "_active";
            this.contentActiveClass = this.wrapper.children[0].classList[0] + "_active";

            this.init();
        }
    }

    setElementsId() {
        Array.from(this.wrapper.children)
            .forEach((tabContent, index) => {
                let contentId = `content-id-${index}`;
                tabContent.setAttribute("id", contentId);
                tabContent.classList.toggle(this.activeContent.classList[0] + '_hidden');
            });

        this.buttons.forEach((button, index) => {
            let tabId = `tab-id-${index}`;
            button.setAttribute("id", tabId);
        });
    }

    init() {
        this.setElementsId();

        this.setActive(
            this.buttons[this.activeTabNumber].id,
            this.wrapper.children[this.activeTabNumber].id
        );

        this.buttons.forEach((button, index) => {
            let tabId = `tab-id-${index}`;
            let contentId = `content-id-${index}`;
            button.addEventListener("click", (event) => {
                this.setActive(tabId, contentId);
            });
        });
    }

    setActive(tabId, contentId) {
        if(this.activeButton && this.activeContent) {
            this.setInactive(this.activeButton, this.activeContent);
        }

        this.activeButton = document.querySelector(`#${tabId}`);
        this.activeContent = document.querySelector(`#${contentId}`);

        this.activeButton.classList.add(this.activeButton.classList[0] + "_active");

        this.activeContent.classList.add(this.activeContent.classList[0] + "_active");
        console.log(this);
    }

    setInactive(activeButton, activeContent) {
        this.activeButton.classList.remove(this.activeButton.classList[0] + "_active");

        this.activeContent.classList.remove(this.activeContent.classList[0] + "_active");
    }
}

export default SimpleTabs;