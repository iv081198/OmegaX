import $ from "jquery";

"use strict";

$(document).ready(function() {
    $(".main-form__wrapper").submit(function() { //Change
      var th = $(this);
      $.ajax({
        type: "POST",
        url: "/mail.php", //Change
        data: th.serialize()
      }).done(function() {
        alert("Спасибо!");
        setTimeout(function() {
          // Done Functions
          th.trigger("reset");
        }, 1000);
      });
      return false;
    });
});

if(window.matchMedia("(max-width: 656px)")) {

  let burger = document.querySelector(".header-nav__burger");
  let menu = document.querySelector(".menu");
  let body = document.querySelector("body");


  burger.addEventListener("click", (event) => {
      burger.classList.toggle("is-active");
      menu.classList.toggle("menu_active");
      body.classList.toggle("body_menu-open");
      document.documentElement.classList.toggle("html_menu-open");
      burger.classList.toggle("header-nav__burger_active");
  });

}


/*
import Module from '../widgets/module/index.js';

import your vue modules example

import Module from '../widgets/module';
const module = new Module('#module-widget');

*/
