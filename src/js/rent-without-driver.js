import $ from "jquery";
import * as datepicker from "@chenfengyuan/datepicker";
import * as datepickerRu from "@chenfengyuan/datepicker/i18n/datepicker.ru-RU";
import MaterialInput from "./material-input";
import SimpleTabs from "./simple-tabs";

"use strict";

let materialInputName = new MaterialInput(
    document.getElementById("material-input-name"),
    {
        activeClass: "material-input_active"
    }
);

let materialInputPhone = new MaterialInput(
    document.getElementById("material-input-phone"),
    {
        activeClass: "material-input_active",
        phone: true
    }
);

let materialTextArea = new MaterialInput(
    document.getElementById("material-textarea"),
    {
        activeClass: "material-textarea_active",
        textarea: true
    }
);

let catalogTabs = new SimpleTabs(
    ".catalog-filters__option",
    ".catalog__tabs",
    {
        defaultTab: 2,
        displayStyle: "flex"
    }
);

let popupInputs = document.querySelectorAll("#material-input-form");

popupInputs.forEach(input => {
    if(input.querySelector("#popup-form-phone")) {
        new MaterialInput(
            input,
            {
                activeClass: "material-input_active",
                phone: true
            }
        );
    } else {
        new MaterialInput(
            input,
            {
                activeClass: "material-input_active"
            }
        );
    }
});

let rentItems = document.querySelectorAll(".catalog-item");

let popupForm = document.querySelector(".popup-form");
let popupCloser = popupForm.querySelector(".popup-form__closer");
let popupOverlay = popupForm.querySelector(".popup-form__overlay");
let popupSelectedAuto = popupForm.querySelector(
    ".popup-form__selected-auto"
);

let popupSelectedAutoInput = document.getElementById(
    "popup-form-selected-auto"
);

$(".popup-form__datepicker-input").datepicker({
    language: 'ru-RU'
});

rentItems.forEach(item => {
    item.addEventListener("click", event => {
        let autoManufacturer = item.querySelector(
            ".catalog-item__manufacturer"
        ).innerText;
        let autoModel= item.querySelector(
            ".catalog-item__model"
        ).innerText;
        let selectedAuto = `${autoManufacturer} ${autoModel}`;

        popupSelectedAuto.innerText = selectedAuto;
        popupSelectedAutoInput.value = selectedAuto;
        popupForm.style.display = "block";
        document.body.classList.toggle("body_menu-open");
        document.documentElement.classList.toggle("html_menu-open");
    });
});

popupCloser.addEventListener("click", event => {
    popupForm.style.display = "none";
    document.body.classList.toggle("body_menu-open");
    document.documentElement.classList.toggle("html_menu-open");
});

popupOverlay.addEventListener("click", event => {
    popupForm.style.display = "none";
    document.body.classList.toggle("body_menu-open");
    document.documentElement.classList.toggle("html_menu-open");
});